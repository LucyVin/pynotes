# PyNotes - A super small note taking shell app

```
$ pynotes Taking a note...
$ pynotes --read
=>[0] 2017-16-10 12:27:40 > Taking a note...
```

### Installation & Usage

`$ git clone https://gitlab.com/lucvin/pynotes && cd pynotes && pip3 install --user .`

Clone the repo and install using Python3's pip.

Once installed, the program can be initiated by calling `pynotes`. If you supply no arguments, it will wait for input before writing to file.

Additionaly, you can read from your list by calling `--read`. 

Lines can be deleted by calling `--del <n>`

Lines can be read individually by passing a number to --read, eg: `pynotes --read 1` will print the 2nd line (default indexing is 0-based)

You can also pass a file path to --read, to read any plain text file. `pynotes --read ~/Downloads/my_friends_notes`

### Configuration

Pynotes can be configured with a .ini file in one of the following locations:

`~/.config/py_notes/config.ini`    
`~/.py_notes.ini`    
`.py_notes.ini`     

If no config is found, it will use default settings.

*Settings:*

+ `file_path` : path to your notes file. Default is `~/.pynotes`
+ `timestamps` : accepts `yes` or `no`. Turns timestamps on or off
+ `timestamp_format` : datetime formatting for the timestamp. You will need to escape percent signs with %%. (eg: `%%Y-%%d-%%m`)
+ `separator` : string you want to separate the datestamp and the note
+ `index` : line number starting point, default is 0-indexed, accepts `1`

example config:

```
[settings]
file_path = ~/.pynotes
timestamps = yes
timestamp_format = %%Y-%%d-%%m %%H:%%M:%%S
separator = >
```
### TODO:

+ Re-implement the whole thing using sqlite backend
+ Add support for file exports
+ Use a proper command line argparser
+ Build out notes API to interact with other programs
