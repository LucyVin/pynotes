
from sys import argv
from sys import stdout
from os import path
from datetime import datetime
from datetime import date
from .config import FILE_PATH
from .config import TIMESTAMPS
from .config import TIMESTAMP_FORMAT
from .config import SEPARATOR
from .config import INDEX

def write_to_note(data):
    notes_file = path.expanduser(FILE_PATH)
    mode = 'a' if path.exists(notes_file) else 'w' # set file.write mode to append or write
    
    with open(notes_file, mode) as f:
        f.write(data)
        f.write('\n')

def note(data):
    if TIMESTAMPS is True:
        timestamp = datetime.now().strftime(TIMESTAMP_FORMAT)
        prepend_str = '{timestamp} {separator} '\
                      .format(timestamp=timestamp,
                              separator=SEPARATOR)
    else:
        prepend_str = ''

    note_data = '{prepend_str}{data}'\
                .format(prepend_str=prepend_str,
                        data=data)

    write_to_note(note_data)

def read(args=None):
    """ naieve command handling, will be re-done when converted to SQLite backend """
    line_number = None
    file_path = path.expanduser(FILE_PATH)

    if args is not None:
        if len(args) > 2:
            stdout.write('Expected at most 2 arguments, got {0}.\n'.format(len(args)))
            return None

        for arg in args:
            if path.exists(arg):
                file_path = path.abspath(arg)
            elif arg.isdigit():
                line_number = int(arg)

    notes_file = file_path

    with open(notes_file, 'r') as f:
        lines = [i for i in f]

    if int(INDEX) != 0:
        for lineno, line in enumerate(lines):
            lines[lineno] = '[{lineno}] {line}'\
                            .format(lineno=lineno+int(INDEX),
                                    line=line)
    else:
        for lineno, line in enumerate(lines):
            lines[lineno] = '[{lineno}] {line}'\
                            .format(lineno=lineno,
                                    line=line)

    if line_number is not None:
        stdout.write(lines[line_number-int(INDEX)])
    else:
        for line in lines:
            stdout.write(line)

def delete(n):
    #TODO: set up delete to delete multiple lines
    n = int(n[0])
    notes_file = path.expanduser(FILE_PATH)
    if int(INDEX) == 1:
        n = n-1
    with open(notes_file, 'r+') as f:
        tmp_list = f.readlines()
        rmd_item = tmp_list.pop(int(n))
        f.seek(0)
        f.truncate()
        for line in tmp_list:
            f.write(line)
        stdout.write('deleted: {item}.\n'.format(item=rmd_item.split('\n')[0]))

command_switch = {'read':read,
                  'del':delete}

def main():
    if len(argv) == 1:
        note_data = input()
        note(note_data)

    elif argv[1][:2] == '--':
        if len(argv) > 2:
            arg_params = argv[2:]
            command_switch[argv[1].split('--')[1]](arg_params)
        else:
            command_switch[argv[1].split('--')[1]]()

    else:
        note_data = ' '.join(argv[1:])
        note(note_data)
