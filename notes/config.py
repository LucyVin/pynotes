
from configparser import ConfigParser
from os import path
from sys import stdout


defaults = {
    'file_path' : path.expanduser('~/Documents/py_notes_file.txt'),
    'timestamps' : True,
    'timestamp_format' : '%d/%m/%y %H:%M:%S',
    'separator' : '|',
    'index' : 0
    }


def get_config(config_path=None):
    """
    loop over config paths for a config.ini or py_notes.ini file,
    if config_path is supplied, get_config will test the path for validity
    """
    config = ConfigParser()

    default_paths = ['~/.config/py_notes/config.ini',
                     '~/.py_notes.ini',
                     'config.ini']

    if config_path is not None:
        config.read(p)
        return config

    for p in default_paths:
        p = path.expanduser(p)
        if path.exists(p):
            config.read(p)
            return config

""" Return None if config is non existant, set default vals """
try:
    config = get_config()
    settings = config['settings']
except:
    config = ConfigParser()
    config['settings'] = defaults
    settings = config['settings']
    stdout.write('found no configuration file, using defaults...\n')

FILE_PATH = settings.get('file_path', defaults['file_path'])
TIMESTAMPS = settings.getboolean('timestamps', defaults['timestamps'])
TIMESTAMP_FORMAT = settings.get('timestamp_format', defaults['timestamp_format'])
SEPARATOR = settings.get('separator', defaults['separator'])
INDEX = settings.get('index', defaults['index'])

if int(INDEX) not in [0, 1]:
    INDEX = 0
