from setuptools import setup

setup(
    name = 'pynotes',
    packages = ['notes'],
    py_modules = ['notes'],
    entry_points = {
        "console_scripts":['pynotes = notes.notes:main']
        },
    version = '0.1.01',
    author = 'lvinc'
)
